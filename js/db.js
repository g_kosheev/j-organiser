function dbInit (callback) {
	db = openDatabase("organiser", "1.01", "Organiser DB", 20000);
	db.transaction(function (tx) {
		tx.executeSql(
			"\
				CREATE TABLE IF NOT EXISTS project (\
					id integer PRIMARY KEY,\
					title string,\
					status integer,\
					x integer,\
					y integer,\
					width integer,\
					height integer\
				);\
		");
		tx.executeSql(
			"\
				CREATE TABLE IF NOT EXISTS task (\
					id integer PRIMARY KEY,\
					title varchar,\
					description text,\
					created timestamp,\
					edited timestamp,\
					closed timestamp,\
					status integer,\
					project_id integer\
				);\
			",
			[],
			function (tx, rs) {
				callback();
			},
			function (tx, e) {
				handleError(e);
			}
		);
	});
}

function getProjects (callback) {
	db.transaction(function (tx) {
		tx.executeSql(
			"SELECT * FROM project WHERE status = 1",
			[],
			function (tx, rs) {
				callback(rs.rows);
			},
			function (tx, e) {
				handleError(e);
			}
		);
	});
}

function addProject (title, callback) {
	db.transaction(function (tx) {
		tx.executeSql(
			"INSERT INTO project (title, status) VALUES (?, 1)",
			[title],
			function (tx, rs) {
				callback();
			},
			function (tx, e) {
				handleError(e);
			}
		);
	});
}

function updateProjectPosition (id, x, y, width, height, callback) {
	db.transaction(function (tx) {
		tx.executeSql(
			"UPDATE project SET x = ?, y = ?, width = ?, height = ? WHERE id = ?",
			[x, y, width, height, id],
			function (tx, rs) {
				if (callback) {
					callback();
				}
			},
			function (tx, e) {
				handleError(e);
			}
		);
	});
}

function getTasks (projectId, callback) {
	db.transaction(function (tx) {
		tx.executeSql(
			"SELECT * FROM task WHERE project_id = ?",
			[projectId],
			function (tx, rs) {
				callback(rs.rows);
			},
			function (tx, e) {
				handleError(e);
			}
		);
	});
}

function addTask (projectId, title, description, callback) {
	db.transaction(function (tx) {
		tx.executeSql(
			"INSERT INTO task (project_id, title, description, status, created) VALUES (?, ?, ?, ?, ?)",
			[projectId, title, description, 0, Math.floor((new Date) / 1000)],
			function (tx, rs) {
				callback();
			},
			function (tx, e) {
				handleError(e);
			}
		);
	});
}

function setTaskStatus (taskId, status, callback) {
	db.transaction(function (tx) {
		tx.executeSql(
			"UPDATE task SET status = ? WHERE id = ?",
			[status, taskId],
			function (tx, rs) {
				callback();
			},
			function (tx, e) {
				handleError(e);
			}
		);
	});
}

