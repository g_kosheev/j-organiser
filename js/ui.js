function drawProject (content, autoPoistion) {
	var $widget = $($('#project-template').html());
	$widget.find('.project-title', $widget).text(content.title);
	$widget.data('project-id', content.id);
	
	grid.addWidget(
		$widget,
		+content.x,
		+content.y,
		+content.width,
		+content.height,
		autoPoistion
	);
	
	redrawTasks(content.id);
}

function drawTask (content, $element) {
	var $task = $($('#task-template').html());
	$task.find('.task-title', $task).text(content.title);
	$task.data('task-id', content.id);
	if (content.status == 1) {
		$task.addClass('task-done');
	}
	
	$element.append($task);
}

function redrawProjects () {
	grid.batchUpdate();
	grid.removeAll();
	getProjects(function (projectsList) {
		for (var i = 0; i < projectsList.length; i++) {
			var projectItem = projectsList[i];
			drawProject(projectItem, false);
		}
	});
	grid.commit();
}

function redrawTasks (projectId) {
	getTasks(projectId, function (tasks) {
		$('.grid-stack-item').each(function (index, projectElement) {
			if ($(projectElement).data('project-id') != projectId) {
				return;
			}
			$('.tasks-wrapper', projectElement).html('');
			for (var i = 0; i < tasks.length; i++) {
				drawTask(tasks[i], $('.tasks-wrapper', projectElement));
			}
		});
	});
}
