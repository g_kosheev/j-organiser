var grid;
$(function () {
	dbInit(function () {
		grid = $('#grid').gridstack({
			cellHeight: 80,
			verticalMargin: 10,
			float: true,
			animate: true
		}).data('gridstack');
		
		$('#grid').on('change', function(event, items) {
			if (!items) {
				return;
			}
			for (var i = 0; i < items.length; i++) {
				updateProjectPosition(
					items[i].el.data('project-id'),
					items[i].x,
					items[i].y,
					items[i].width,
					items[i].height
				)
			}
		});
		
		$('#grid').on('click', '.project-item .button-add-task', function () {
			var projectId = $(this).closest('.grid-stack-item').data('project-id');
			$('#task-title').val('');
			$('#task-description').val('');
			var dialog = $( "#task-dialog" ).dialog({
				autoOpen: true,
				height: 400,
				width: 350,
				modal: true,
				buttons: {
					"Create": function() {
						addTask(
							projectId,
							$('#task-title').val(),
							$('#task-description').val(),
							function () {
								redrawTasks(projectId);
							}
						);
						dialog.dialog( "close" );
					}
				},
				close: function() {
				}
			});
		});
		
		$('#grid').on('click', '.task-item .button-task-done', function () {
			var taskId = $(this).closest('.task-item').data('task-id');
			var projectId = $(this).closest('.grid-stack-item').data('project-id');
			setTaskStatus(taskId, 1, function () {
				redrawTasks(projectId);
			});
		});
		
		$('#button-add-project').on('click', function () {
			var dialog = $( "#add-project-dialog" ).dialog({
				autoOpen: true,
				height: 400,
				width: 350,
				modal: true,
				buttons: {
					"Create": function() {
						addProject(
							$('#new-project-name').val(),
							redrawProjects
						);
						dialog.dialog( "close" );
					}
				},
				close: function() {
				}
			});
		});
		
		redrawProjects();
	});
});
